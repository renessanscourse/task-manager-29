package ru.ovechkin.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.endpoint.IStorageEndpoint;
import ru.ovechkin.tm.api.service.ISessionService;
import ru.ovechkin.tm.api.service.IStorageService;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.dto.SessionDTO;
import ru.ovechkin.tm.enumirated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.JAXBException;
import java.io.*;

@WebService
public class StorageEndpoint extends AbstractEndpoint implements IStorageEndpoint {

    private IStorageService storageService;

    private ISessionService sessionService;

    public StorageEndpoint() {
        super(null);
    }

    public StorageEndpoint(final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.storageService = serviceLocator.getStorageService();
        this.sessionService = serviceLocator.getSessionService();
    }

    @Override
    @WebMethod
    public void dataBase64Save(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataBase64Save();
    }

    @Override
    @WebMethod
    public void dataBase64Load(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws Exception {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataBase64Load();
    }

    @Override
    @WebMethod
    public void dataBinarySave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataBinarySave();
    }

    @Override
    @WebMethod
    public void dataBinaryLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, ClassNotFoundException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataBinaryLoad();
    }

    @Override
    @WebMethod
    public void dataJsonJaxbSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, JAXBException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonJaxbSave();
    }

    @Override
    @WebMethod
    public void dataJsonJaxbLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws JAXBException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonJaxbLoad();
    }

    @Override
    @WebMethod
    public void dataJsonMapperLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonMapperLoad();
    }

    @Override
    @WebMethod
    public void dataJsonMapperSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataJsonMapperSave();
    }

    @Override
    @WebMethod
    public void dataXmlJaxbSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, JAXBException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlJaxbSave();
    }

    @Override
    @WebMethod
    public void dataXmlJaxbLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException, JAXBException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlJaxbLoad();
    }

    @Override
    @WebMethod
    public void dataXmlMapperSave(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlMapperSave();
    }

    @Override
    @WebMethod
    public void dataXmlMapperLoad(
            @WebParam(name = "session", partName = "session") final SessionDTO sessionDTO
    ) throws IOException {
        serviceLocator.getSessionService().validate(sessionDTO, Role.ADMIN);
        storageService.dataXmlMapperLoad();
    }

    @NotNull
    @Override
    @WebMethod
    public Integer getServerPortInfo() {
        return serviceLocator.getPropertyService().getServicePort();
    }

    @NotNull
    @Override
    @WebMethod
    public String getServerHostInfo() {
        return serviceLocator.getPropertyService().getServiceHost();
    }

}