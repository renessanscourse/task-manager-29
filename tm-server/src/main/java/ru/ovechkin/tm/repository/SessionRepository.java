package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.entity.Session;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

public final class SessionRepository implements ISessionRepository {

    @NotNull
    private EntityManager entityManager;

    public SessionRepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) {
        entityManager.persist(session);
        return session;
    }

    @Nullable
    public Session findById(@NotNull final String id) {
        return entityManager.find(Session.class, id);
    }

    @Nullable
    public List<Session> findByUserId(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) return new ArrayList<>();
        final Query query = entityManager.createQuery(
                "from Session where user_id = :userId", Session.class);
        query.setParameter("userId", userId);
        if (query.getResultList() == null || query.getResultList().isEmpty()) return null;
        @Nullable final List<Session> session = (List<Session>) query.getResultList();
        return session;
    }

    public void remove(@NotNull final Session session) {
        entityManager.remove(session);
    }

    public void removeByUserId(@Nullable final String userId) {
        @NotNull final Query query = entityManager.createQuery("delete Session where user_id = :userId");
        query.setParameter("userId", userId);
        query.executeUpdate();
    }

    public boolean contains(@NotNull final String id) {
        @Nullable final Session session = findById(id);
        return entityManager.contains(session);
    }

}