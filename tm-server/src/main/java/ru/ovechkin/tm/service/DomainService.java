package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.service.*;
import ru.ovechkin.tm.dto.Domain;

public final class DomainService implements IDomainService {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final IUserService userService;

    public DomainService(
            @NotNull final ITaskService taskService,
            @NotNull final IProjectService projectService,
            @NotNull final IUserService userService
    ) {
        this.taskService = taskService;
        this.projectService = projectService;
        this.userService = userService;
    }

    @Override
    public void load(@Nullable final Domain domain) {
        if (domain == null) return;
        projectService.loadProjects(domain.getProjects());
        taskService.loadTasks(domain.getTasks());
        userService.loadUsers(domain.getUsers());
    }

    @Override
    public void save(@Nullable final Domain domain) {
        if (domain == null) return;
        domain.setProjects(projectService.getAllProjectsDTO());
        domain.setTasks(taskService.getAllTasksDTO());
        domain.setUsers(userService.getAllUsersDTO());
    }

}