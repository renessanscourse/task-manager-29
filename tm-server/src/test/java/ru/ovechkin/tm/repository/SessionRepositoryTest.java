package ru.ovechkin.tm.repository;

import org.junit.Test;
import ru.ovechkin.tm.api.locator.IServiceLocator;
import ru.ovechkin.tm.api.repository.ISessionRepository;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.IEntityManagerService;
import ru.ovechkin.tm.dto.UserDTO;
import ru.ovechkin.tm.entity.Session;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.locator.ServiceLocator;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepositoryTest {

    private final IServiceLocator serviceLocator = new ServiceLocator();

    private final IEntityManagerService entityManagerService = serviceLocator.getEntityManagerService();

    {
        try {
            entityManagerService.init();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final EntityManager entityManager = entityManagerService.getEntityManager();

    private final ISessionRepository sessionRepository = new SessionRepository(entityManager);

    @Test
    public void testAdd() {
        final IUserRepository userRepository = new UserRepository(entityManager);
        final User user = userRepository.findByLogin("adminForRemove");
        final Session session = new Session();
        session.setUser(user);
        session.setRole(user.getRole());
        sessionRepository.add(session);
    }

    @Test
    public void testFindById() {
        final Session session = sessionRepository.findById("97016a38-7d3c-4d27-8da8-9b7942e3b375");
        if (session == null) throw new RuntimeException("Session not founded...");
        final UserDTO userDTO = new UserDTO(session.getUser());
        System.out.println(session.getId());
        System.out.println(userDTO);
    }

    @Test
    public void testFindAllSessions() {
        final IUserRepository userRepository = new UserRepository(entityManager);
        final User user = userRepository.findByLogin("adminForRemove");
        final List<Session> sessions = sessionRepository.findByUserId(user.getId());
        System.out.println(user.getSessions());//1 способ
        System.out.println(sessions);//2 способ
    }

    @Test
    public void removeAllSession() {
        final IUserRepository userRepository = new UserRepository(entityManager);
        final User user = userRepository.findByLogin("adminForRemove");
        sessionRepository.removeByUserId(user.getId());
    }

    @Test
    public void testContains() {
        System.out.println(sessionRepository.contains("1b02198c-4136-4a41-9658-77b55e03bd11"));
    }

}