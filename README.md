# PROJECT INFO

TASK MANAGER

# DEVELOPER INFO

**NAME**: OVECHKIN ROMAN

**E-MAIL**: ovechkin@roman.ru

# SOFTWARE

- JDK 1.8

- Windows 10

# PROGRAM BUILD

```bash
mvn clean install
```

# PROGRAM RUN

```bash
java -jar ./taskDTO-manager.jar
```

# SCREENSHOTS
СкринШот Работы Компоуза Локально - https://yadi.sk/d/zFGBp26eflft1g?w=1
