package ru.ovechkin.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.api.locator.IEndpointLocator;
import ru.ovechkin.tm.endpoint.SessionDTO;
import ru.ovechkin.tm.enumirated.Role;

public abstract class AbstractCommand {

    protected IEndpointLocator endpointLocator;

    protected SessionDTO sessionDTO;

    public void setEndpointLocator(
            @NotNull IEndpointLocator endpointLocator,
            @NotNull SessionDTO sessionDTO
    ) {
        this.endpointLocator = endpointLocator;
        this.sessionDTO = sessionDTO;
    }

    public Role[] roles () {
        return null;
    }

    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws Exception;

    public AbstractCommand() {
    }

}