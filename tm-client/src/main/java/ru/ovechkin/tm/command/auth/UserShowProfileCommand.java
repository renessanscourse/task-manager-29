package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.endpoint.UserDTO;

public class UserShowProfileCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.SHOW_PROFILE;
    }

    @NotNull
    @Override
    public String description() {
        return "Show information about your account";
    }

    @Override
    public void execute() {
        @Nullable final UserDTO userDTO = endpointLocator.getSessionEndpoint().getUser(sessionDTO);
        System.out.println("[PROFILE INFORMATION]");
        System.out.println("YOUR LOGIN IS: " + userDTO.getLogin());
        System.out.println("YOUR FIRST NAME IS: " + userDTO.getFirstName());
        System.out.println("YOUR MIDDLE NAME IS: " + userDTO.getMiddleName());
        System.out.println("YOUR LAST NAME IS: " + userDTO.getLastName());
        System.out.println("YOUR EMAIL IS: " + userDTO.getEmail());
        System.out.println("YOUR ROLE IS: " + userDTO.getRole());
        System.out.println("[OK]");
    }

}